package Almacen;
import java.util.Scanner;

public class Almacen {
	static Electrodomesticos[] estante = new Electrodomesticos[1];
	static Scanner tcl =new Scanner(System.in);
	
	static void crear() {
		System.out.println("Por ahora solo puedes crear:");
		System.out.println(" 1 Aspiradoras\n 2 DvD\n 3 Lavadoras\n 4 Licuadoras\n 5 Neveras\n 6 Televisores");
		int election = tcl.nextInt();
			switch(election) {
			case 1:
				System.out.println("Ingresa el nombre de la Aspiradora:");
				tcl.nextLine();
				String nombre = tcl.nextLine();
				System.out.println("Ingresa la marca de la Aspiradora:");
				String marca = tcl.nextLine();
				System.out.println("Ingresa la color de la Aspiradora:");
				String color = tcl.nextLine();
				System.out.println("Ingresa el precio de la Aspiradora, esta sera un numero en pesos colombianos:");
				int precio = tcl.nextInt();
				System.out.println("Ingresa el numero de serie de la Aspirdora:");
				int serie = tcl.nextInt();
				System.out.println("La Aspiradora es inalambrica? Si/No");
				tcl.nextLine();
				String inal = tcl.nextLine();
				if(!inal.equals("Si") && !inal.equals("No")) {
					System.out.println("Error en eleccion por favor crea de nuevo la Aspiradora \n");
					crear();
					break;
				}
				System.out.println("Ingrese la capacidad de la bolsa, este numero sera los kilos que esta aguanta:");
				float capacidadBolsa = tcl.nextFloat();
				System.out.println("Ingrese la potencia de la Aspiradora");
				int potencia = tcl.nextInt();
				System.out.println("");
				Aspiradoras aspiradora = new Aspiradoras(nombre, marca, color, precio, serie, inal, capacidadBolsa, potencia, "Aspiradora");
				int length = estante.length - 1;
				estante[length] = aspiradora;
				Electrodomesticos[] newEstante = new Electrodomesticos[estante.length + 1];
				for(int i = 0;i<estante.length;i++) {
					newEstante[i] = estante[i];
				}
				estante = newEstante;
				System.out.println("Has creado la Aspiradora: " + estante[estante.length-2].getNombre()+"\n");
				break;
			case 2:
				System.out.println("Ingresa el nombre el DvD:");
				tcl.nextLine();
				nombre = tcl.nextLine();
				System.out.println("Ingresa la marca de el DvD:");
				marca = tcl.nextLine();
				System.out.println("Ingresa la color de el DvD:");
				color = tcl.nextLine();
				System.out.println("Ingresa el precio de el DvD, esta sera un numero en pesos colombianos:");
				precio = tcl.nextInt();
				System.out.println("Ingresa el numero de serie de el DvD:");
				serie = tcl.nextInt();
				System.out.println("el DvD usa bluray? Si/No");
				tcl.nextLine();
				inal = tcl.nextLine();
				if(!inal.equals("Si") && !inal.equals("No")) {
					System.out.println("Error en eleccion por favor crea de nuevo el DvD \n");
					crear();
					break;
				}
				System.out.println("Ingrese la version del DvD:");
				float version = tcl.nextFloat();
				System.out.println("");
				DvD dvd = new DvD(nombre, marca, color, precio, serie, version, inal,"DvD");
				length = estante.length - 1;
				estante[length] = dvd;
				newEstante = new Electrodomesticos[estante.length + 1];
				for(int i = 0;i<estante.length;i++) {
					newEstante[i] = estante[i];
				}
				estante = newEstante;
				System.out.println("Has creado el DVD: " + estante[estante.length-2].getNombre()+"\n");
				break;
			case 3:
				System.out.println("Ingresa el nombre de la Lavadora:");
				tcl.nextLine();
				nombre = tcl.nextLine();
				System.out.println("Ingresa la marca de de la Lavadora:");
				marca = tcl.nextLine();
				System.out.println("Ingresa la color de de la Lavadora:");
				color = tcl.nextLine();
				System.out.println("Ingresa el precio de de la Lavadora, esta sera un numero en pesos colombianos:");
				precio = tcl.nextInt();
				System.out.println("Ingresa el numero de serie de la Lavadora:");
				serie = tcl.nextInt();
				System.out.println("Ingrese la capacidad de agua de la Lavadora:");
				float capAgua = tcl.nextFloat();
				System.out.println("");
				Lavadoras lavadora = new Lavadoras(nombre, marca, color, precio, serie,capAgua,"Lavadora");
				length = estante.length - 1;
				estante[length] = lavadora;
				newEstante = new Electrodomesticos[estante.length + 1];
				for(int i = 0;i<estante.length;i++) {
					newEstante[i] = estante[i];
				}
				estante = newEstante;
				System.out.println("Has creado la Lavadora: " + estante[estante.length-2].getNombre()+"\n");
				break;
			case 4:
				System.out.println("Ingresa el nombre la Licuadora:");
				tcl.nextLine();
				nombre = tcl.nextLine();
				System.out.println("Ingresa la marca de la Licuadora:");
				marca = tcl.nextLine();
				System.out.println("Ingresa la color de la Licuadora:");
				color = tcl.nextLine();
				System.out.println("Ingresa el precio de la Licuadora, esta sera un numero en pesos colombianos:");
				precio = tcl.nextInt();
				System.out.println("Ingresa el numero de serie de la Licuadora:");
				serie = tcl.nextInt();
				System.out.println("la Licuadora viene con vaso incluido? Si/No");
				tcl.nextLine();
				inal = tcl.nextLine();
				if(!inal.equals("Si") && !inal.equals("No")) {
					System.out.println("Error en eleccion por favor crea de nuevo la Licuadora\n");
					crear();
					break;
				}
				System.out.println("Ingrese la potencia la Licuadora:");
				potencia = tcl.nextInt();
				System.out.println("");
				Licuadoras licuadora = new Licuadoras(nombre, marca, color, precio, serie,inal,potencia,"Licuadora");
				length = estante.length - 1;
				estante[length] = licuadora;
				newEstante = new Electrodomesticos[estante.length + 1];
				for(int i = 0;i<estante.length;i++) {
					newEstante[i] = estante[i];
				}
				estante = newEstante;
				System.out.println("Has creado el DVD: " + estante[estante.length-2].getNombre()+"\n");
				break;
			case 5:
				System.out.println("Ingresa el nombre la Nevera:");
				tcl.nextLine();
				nombre = tcl.nextLine();
				System.out.println("Ingresa la marca de la Nevera:");
				marca = tcl.nextLine();
				System.out.println("Ingresa la color de la Nevera:");
				color = tcl.nextLine();
				System.out.println("Ingresa el precio de la Nevera, esta sera un numero en pesos colombianos:");
				precio = tcl.nextInt();
				System.out.println("Ingresa el numero de serie de la Nevera:");
				serie = tcl.nextInt();
				System.out.println("la Nevera viene con dispensador de agua? Si/No");
				tcl.nextLine();
				inal = tcl.nextLine();
				if(!inal.equals("Si") && !inal.equals("No")) {
					System.out.println("Error en eleccion por favor crea de nuevo la Nevera\n");
					crear();
					break;
				}
				Neveras nevera = new Neveras(nombre, marca, color, precio, serie,inal,"Nevera");
				length = estante.length - 1;
				estante[length] = nevera;
				newEstante = new Electrodomesticos[estante.length + 1];
				for(int i = 0;i<estante.length;i++) {
					newEstante[i] = estante[i];
				}
				estante = newEstante;
				System.out.println("Has creado la Nevera: " + estante[estante.length-2].getNombre()+"\n");
				break;
			case 6:
				System.out.println("Ingresa el nombre el Televisor:");
				tcl.nextLine();
				nombre = tcl.nextLine();
				System.out.println("Ingresa la marca del Televisor:");
				marca = tcl.nextLine();
				System.out.println("Ingresa la color del Televisor:");
				color = tcl.nextLine();
				System.out.println("Ingresa el precio del Televisor, esta sera un numero en pesos colombianos:");
				precio = tcl.nextInt();
				System.out.println("Ingresa el numero de serie del Televisor:");
				serie = tcl.nextInt();
				System.out.println("el Televisor tiene 4K? Si/No");
				tcl.nextLine();
				inal = tcl.nextLine();
				if(!inal.equals("Si") && !inal.equals("No")) {
					System.out.println("Error en eleccion por favor crea de nuevo el Televisor\n");
					crear();
					break;
				}
				System.out.println("Ingrese las pulgadas del Televisor:");
				int pulgadas = tcl.nextInt();
				System.out.println("");
				Televisores tv = new Televisores(nombre, marca, color, precio, serie,inal,pulgadas,"Televisor");
				length = estante.length - 1;
				estante[length] = tv;
				newEstante = new Electrodomesticos[estante.length + 1];
				for(int i = 0;i<estante.length;i++) {
					newEstante[i] = estante[i];
				}
				estante = newEstante;
				System.out.println("Has creado el DVD: " + estante[estante.length-2].getNombre()+"\n");
				break;
			}		
	}
	static void buscar() {
		System.out.println("Deseas buscar por:");
		System.out.println(" 1 Nombre\n 2 Tipo\n 3 Precio\n 4 Marca");
		int election = tcl.nextInt();
		switch(election) {
		case 1:
			System.out.println("Escribe el nombre del producto que deseas buscar, recuerda que este tiene que ser exacto");
			tcl.nextLine();
			String buscar = tcl.nextLine();
			System.out.println("Se han encontrado estos electrodomesticos");
			for(int i = 0;i<estante.length-1;i++) {
				if(estante[i].getNombre().equals(buscar)) {
				}else {
					System.out.println("\nNo hay resultados");
				}
			}
			break;
		case 2:
			System.out.println("Escribe el tipo de producto que deseas buscar, recuerda que tenemos Televisor, Licuadora, Aspiradora, DvD, Lavadoras, Neveras");
			tcl.nextLine();
			buscar = tcl.nextLine();
			System.out.println("Se han encontrado estos electrodomesticos");
			for(int i = 0;i<estante.length-1;i++) {
				if(estante[i].getTipo().equals(buscar)) {
					System.out.println("Nombre: " + estante[i].getNombre() + " Precio: " + estante[i].getPrecio() + " Color: " + estante[i].getColor() +  " Marca: " + estante[i].getMarca() + " Tipo: " + estante[i].getTipo());
				}else {
					System.out.println("\nNo hay resultados");
				}
			}
			break;
		case 3:
			System.out.println("Escribe el precio del producto que deseas buscar, este tiene que ser exacto");
			tcl.nextLine();
			int buscarp = tcl.nextInt();
			System.out.println("Se han encontrado estos electrodomesticos");
			for(int i = 0;i<estante.length-1;i++) {
				if(estante[i].getPrecio() == buscarp) {
					System.out.println("Nombre: " + estante[i].getNombre() + " Precio: " + estante[i].getPrecio() + " Color: " + estante[i].getColor() +  " Marca: " + estante[i].getMarca() + " Tipo: " + estante[i].getTipo());
				}else {
					System.out.println("\nNo hay resultados");
				}
			}
			break;
		case 4:
			System.out.println("Escribe la marca del producto que deseas buscar, recuerda que tiene que ser exacto");
			tcl.nextLine();
			buscar = tcl.nextLine();
			System.out.println("Se han encontrado estos electrodomesticos");
			for(int i = 0;i<estante.length-1;i++) {
				if(estante[i].getMarca().equals(buscar)) {
					System.out.println("Nombre: " + estante[i].getNombre() + " Precio: " + estante[i].getPrecio() + " Color: " + estante[i].getColor() +  " Marca: " + estante[i].getMarca() + " Tipo: " + estante[i].getTipo());
				}else {
					System.out.println("\nNo hay resultados");
				}
			}
			break;
		}
		
	}
	
	public static void main(String[] args) {
		System.out.println("Hola bienvenido a tu almacen");
		int i = 2;
		while(i == 2){
			System.out.println("\nSi deseas buscar un producto digita *Buscar*");
			System.out.println("Si deseas agregar un producto digita *Agregar*");
			String eleccion = tcl.nextLine();
			switch(eleccion) {
			case "Buscar":
				buscar();
				break;
			case "Agregar":
				crear();
				break;
				}
			}	
		}
}

