package Almacen;

public class Aspiradoras extends Electrodomesticos {
	private int potencia;
	private float capBolsa;
	private String inalambrica;
	public Aspiradoras(String nombre, String marca,String color,int precio, int serie,String inalambrica, float capBolsa,int potencia,String tipo) {
		super(nombre,marca,color,precio,serie,tipo);
		this.capBolsa = capBolsa;
		this.potencia = potencia;
		this.inalambrica = inalambrica;
	}
	public float getCapBolsa() {
		return capBolsa;
	}
	public void setCapBolsa(float capBolsa) {
		this.capBolsa = capBolsa;
	}
	public int getPotencia() {
		return potencia;
	}
	public void setPotencia(int potencia) {
		this.potencia = potencia;
	}
	public String isInalambrica() {
		return inalambrica;
	}
	public void setInalambrica(String inalambrica) {
		this.inalambrica = inalambrica;
	}
	
}
