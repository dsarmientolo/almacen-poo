package Almacen;

public class DvD extends Electrodomesticos {
	private float version;
	private String bluray;
	public DvD(String nombre, String marca,String color,int precio, int serie,float version,String bluray,String tipo) {
		super(nombre,marca,color,precio,serie,tipo);
		this.version = version;
		this.bluray = bluray;
	}
	
	public float getVersion() {
		return version;
	}
	public void setVersion(float version) {
		this.version = version;
	}
	public String isBluray() {
		return bluray;
	}
	public void setBluray(String bluray) {
		this.bluray = bluray;
	}
	
}
