package Almacen;

public abstract class Electrodomesticos {
	private String nombre,marca,color,tipo;
	private int serie,precio;
	
	public Electrodomesticos(String nombre,String marca,String color,int serie, int precio,String tipo) {
		this.nombre = nombre;
		this.marca =  marca;
		this.color = color;
		this.serie = serie;
		this.precio = precio;
		this.tipo = tipo;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getMarca() {
		return marca;
	}

	public void setMarca(String marca) {
		this.marca = marca;
	}

	public String getColor() {
		return color;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public int getSerie() {
		return serie;
	}

	public void setSerie(int serie) {
		this.serie = serie;
	}

	public int getPrecio() {
		return precio;
	}

	public void setPrecio(int precio) {
		this.precio = precio;
	}
	
	
}

