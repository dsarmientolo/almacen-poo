package Almacen;

public class Lavadoras extends Electrodomesticos {
	private float capAgua;
	
	public Lavadoras(String nombre, String marca,String color,int precio, int serie, float capAgua,String tipo) {
		super(nombre,marca,color,precio,serie,tipo);
		this.capAgua =  capAgua;
	}

	public float getCapAgua() {
		return capAgua;
	}

	public void setCapAgua(float capAgua) {
		this.capAgua = capAgua;
	}
	

}
