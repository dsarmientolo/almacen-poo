package Almacen;

public class Licuadoras extends Electrodomesticos{
	private int potencia;
	private String vaso;

	public Licuadoras(String nombre, String marca,String color,int precio, int serie, String vaso, int potencia,String tipo){
		super(nombre,marca,color,precio,serie,tipo);
		this.potencia = potencia;
		this.vaso = vaso;
	}

	public int getPotencia() {
		return potencia;
	}

	public void setPotencia(int potencia) {
		this.potencia = potencia;
	}

	public String isVaso() {
		return vaso;
	}

	public void setVaso(String vaso) {
		this.vaso = vaso;
	}
	
}
