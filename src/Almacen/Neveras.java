package Almacen;

public class Neveras extends Electrodomesticos {
	private String dispenAgua;
	
	public Neveras(String nombre, String marca,String color,int precio, int serie,String dispenAgua,String tipo) {
		super(nombre,marca,color,precio,serie,tipo);
		this.dispenAgua = dispenAgua;
	}

	public String isDispenAgua() {
		return dispenAgua;
	}

	public void setDispenAgua(String dispenAgua) {
		this.dispenAgua = dispenAgua;
	}
	
}
