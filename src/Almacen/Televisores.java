package Almacen;

public class Televisores extends Electrodomesticos {
	private int pulgadas;
	private String cuatroK;
	
	public Televisores(String nombre, String marca,String color,int precio, int serie,String cuatroK,int pulgadas,String tipo){
		super(nombre,marca,color,precio,serie,tipo);
		this.pulgadas = pulgadas;
		this.cuatroK = cuatroK;
	}
	
	
	public int getPulgadas() {
		return pulgadas;
	}


	public void setPulgadas(int pulgadas) {
		this.pulgadas = pulgadas;
	}


	public String isCuatroK() {
		return cuatroK;
	}


	public void setCuatroK(String cuatroK) {
		this.cuatroK = cuatroK;
	}


}
